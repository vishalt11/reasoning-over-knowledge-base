require 'torch'
require 'nn'
--Freebase First
--Getting entities and converting them to vectors
local file = io.open("freebase_common_entity.txt")
vocab_entities = {}
count = 1
if file then
	for line in file:lines() do
		vocab_entities[line]  = count
		count = count +1
	end
else
end

--print(vocab_entities)
vocab_entities_size = count -1


--Getting relations and converting them to vectors
local file = io.open("freebase_relation.txt")
vocab_relation = {}
count = 1
if file then
	for line in file:lines() do
		vocab_relation[line]  = count
		count = count +1
	end
else
end

--print(vocab_relation)
vocab_entities_size = count -1


--convert traingin data to vector format
local file = io.open("data/Freebase/train.txt")
count = 0
if file then
	for line in file:lines() do
		count = count +1
	end
else
end
train = {}

training_data_size = count

local file = io.open("data/Freebase/train.txt")
counter = 0
for line in file:lines() do
	train_temp= {}
	couter = counter +1
	count = 0
	entity1_table = ''
	entity2_table = ''
	relation_table =''
	for c in line:gmatch"." do
		if c == '\t' then
			count = count +1
		else
		end
		if count == 0 then
			entity1_table = entity1_table .. tostring(c)
		elseif count == 1 then
			if c == '\t' then
			else
				relation_table = relation_table .. tostring(c)
			end
		else
			if c == '\t' then
			else
				entity2_table = entity2_table .. tostring(c)
			end
		end
	end
	--print(line)
	--print(entity1_table)
	--print(entity2_table)
	--print(relation_table)
	--print(type(vocab_entities[entity1_table]))
	table.insert(train_temp, vocab_entities[entity1_table])
	table.insert(train_temp, vocab_relation[relation_table])
	table.insert(train_temp, vocab_entities[entity2_table])
	--print(train_temp)
	--train[counter][1] = vocab_entities[entity1_table]
	--train[counter][2] = vocab_relation[relation_table]
	--train[counter][3] = vocab_entities[entity2_table]
	table.insert(train,train_temp)
	--break
end
--print(train)



--convert validation data to vector format
local file = io.open("data/Freebase/dev.txt")
count = 0
if file then
	for line in file:lines() do
		count = count +1
	end
else
end
dev = {}


dev_data_size = count

local file = io.open("data/Freebase/dev.txt")
counter = 0
for line in file:lines() do
	dev_temp= {}
	couter = counter +1
	count = 0
	entity1_table = ''
	entity2_table = ''
	relation_table =''
	output = 0
	for c in line:gmatch"." do
		if c == '\t' then
			count = count +1
		else
		end
		if count == 0 then
			entity1_table = entity1_table .. tostring(c)
		elseif count == 1 then
			if c == '\t' then
			else
				relation_table = relation_table .. tostring(c)
			end
		elseif count == 2 then
			if c == '\t' then
			else
				entity2_table = entity2_table .. tostring(c)
			end
		else
			if c == '\t' then
			else
				if c == '-' then
					output = -1
					break
				else
					output = 1
				end
			end
		end
	end
	--print(line)
	--print(count)
	--print(entity1_table)
	--print(entity2_table)
	--print(relation_table)
	--print(output)
	table.insert(dev_temp, vocab_entities[entity1_table])
	table.insert(dev_temp, vocab_relation[relation_table])
	table.insert(dev_temp, vocab_entities[entity2_table])
	table.insert(dev_temp, output)
	--print(train_temp)
	--train[counter][1] = vocab_entities[entity1_table]
	--train[counter][2] = vocab_relation[relation_table]
	--train[counter][3] = vocab_entities[entity2_table]
	table.insert(dev,dev_temp)
end
--print(dev)

--Preparing the dataset
--dataset training
dataset = {}
function dataset:size() return dev_data_size end
--for i = 1, training_data_size do
--    input = torch.Tensor{train[i][1], train[i][2], train[i][3]}
--    output = 1
--    table.insert(dataset,{input,output})
--end

for i = 1, dev_data_size do
    input = torch.Tensor{dev[i][1], dev[i][2], dev[i][3]}
    output = torch.Tensor{dev[i][4]}
    --table.insert(dataset,{input,output})
    dataset[i] = {input, output}
end
print(dataset:size())


--for 
--input_1 = torch.Tensor{vocab['i'], vocab['nlp']}


print("Starting to train the nn")
model = nn.Sequential()
inputs=3; outputs=1; HUs=20;
model:add(nn.Linear(inputs,HUs))
model:add(nn.Tanh())
model:add(nn.Linear(HUs,outputs))


--criterion = nn.ClassNLLCriterion()
criterion = nn.MSECriterion()  
trainer = nn.StochasticGradient(model, criterion)
trainer.learningRate = 0.001
trainer:train(dataset)


--x=torch.Tensor(3); x[1]=12; x[2]=2; x[3]=562; print(model:forward(x))

