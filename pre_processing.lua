require 'torch'
require 'nn'
print("Starting to train the nn")
model = nn.Sequential()
inputs=3; outputs=1; HUs=20;
model:add(nn.Linear(inputs,HUs))
model:add(nn.Tanh())
model:add(nn.Linear(HUs,outputs))


criterion = nn.ClassNLLCriterion()
--criterion = nn.MSECriterion()  
trainer = nn.StochasticGradient(model, criterion)
trainer.learningRate = 0.01